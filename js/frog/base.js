/* Frog framework - base package */

(function (exports) {
	function resolve(path, obj) {
		var levels = path.split(".");
		for (var i = 0; i < levels.length; i++) {
			var level = levels[i];
			obj[level] = obj[level] || {};
			obj = obj[level];
		}
		return obj;
	}

	var base = resolve("frog.base", exports);
	
	base.resolve = resolve;
	
	base.isFunction = function (obj) {
		return obj && typeof obj === "function";
	};

	base.in = function (arr, val) {
		return arr.indexOf(val) !== -1;
	};

	base.forEach = function (arr, callback, filter) {
		var filter = filter || function () { return true; };
		for (var key in arr) {
			if (filter(key, arr[key])) {
				callback(key, arr[key]);
			}
		}
	};

	base.randomInt = function (min, max) {
		return Math.random() * (max - min) + min;
	};
})(window);