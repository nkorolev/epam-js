(function (exports) {
	var base = exports.frog.base.resolve("frog.base", exports);
	var oop = base.resolve("frog.oop", exports);
	var ui = base.resolve("frog.ui", exports);
	var component = base.resolve("frog.ui.component", exports);

	component.ProgressButton = oop.Class({
		constructor: function () {
			this._element = this._bar = this._content = this._click = this._done = null;
			this._percent = 0;
		},
		attach: function (toElement) {
			this._element = toElement;
			//prepare
			var template = "<span class=\"content\">{{text}}</span><span class=\"progress-inner\"></span>";
			this._element.innerHTML = ui.render(template, {text: this._element.textContent});
			this._bar = this._element.querySelector(".progress-inner");
			this._content = this._element.querySelector(".content");
		},
		setPercent: function (percent) {
			if (percent < 0) {
				throw new Error("Value must be more than 0.");
			}
			if (percent >= 100) {
				percent = 100;
				if (this._done) {
					this._done();
				}
			}
			
			this._percent = Math.round(percent);
			this.setText(this._percent + "%");
			this.setProgress(this._percent + "%");
		},
		addPercent: function (add) {
			this.setPercent(this._percent + add);
		},
		getElement: function () {
			return this._element;
		},
		setText: function (text) {
			this._content.innerText = text;
		},
		setProgress: function (progress) {
			this._bar.style.width = progress;
		},
		isDone: function () {
			return this._percent === 100;
		},
		reset: function() {
			this._percent = 0;
			this.setProgress(this._percent);
		},
		onClick: function (handler) {
			this._click = handler.bind(this);
			this._element.addEventListener("click", this._click);
		},
		onDone: function (handler) {
			this._done = handler.bind(this);
		}
	});
})(window);