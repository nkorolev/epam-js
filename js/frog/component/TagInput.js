(function (exports) {
	var base = exports.frog.base.resolve("frog.base", exports);
	var oop = base.resolve("frog.oop", exports);
	var ui = base.resolve("frog.ui", exports);
	var component = base.resolve("frog.ui.component", exports);

	var Tag = oop.Class({
		constructor: function (text, tagInput) {
			this._text = text;
			this._tagInput = tagInput;
			this._element = document.createElement("li");
			this._element.className = "tag";
			this._element.innerHTML = ui.render("<span class='text'>{{text}}</span> <span class='close'>x</span>", {
				text: this._text
			});
			this.closeClickHandler = this.closeClickHandler.bind(this);
			this._element.querySelector("span.close").addEventListener("click", this.closeClickHandler);
		},
		getText: function () {
			return this._text;
		},
		getElement: function() {
			return this._element;
		},
		getTagInput: function() {
			return this._tagInput;
		},
		closeClickHandler: function (ev) {
			this.getTagInput().removeTag(this);
		}
	});
	
	component.TagInput = oop.Class({
		constructor: function() {
			this._container = this._input = null;
			this._tags = {};
			this._order = [];
			this.inputHandler = this.inputHandler.bind(this);
			this.backspaceHandler = this.backspaceHandler.bind(this);
		},
		DELIMITERS: [";", "."],
		attach: function(element) {
			element.className += " tag-input";
			element.innerHTML = ui.render("<ul class='tag-list'><li><input type='text' placeholder='Input here' autofocus /></li></ul>");
			this._container = element.querySelector(".tag-list");
			this._input = this._container.querySelector("input");
			this._input.addEventListener("keypress", this.inputHandler);
			this._input.addEventListener("keydown", this.backspaceHandler);
		},
		getText: function() {
			return this._input.value;
		},
		setText: function(text) {
			this._input.value = text;
		},
		clear: function () {
			this.setText("");
		},
		addTag: function (tag) {
			var text = tag.getText();
			if (text.length === 0) {
				throw new Error("Метка не может быть пустой.");
			}
			if (this.isExist(text)) {
				throw new Error("Такая метка уже существует.");
			}
			
			this._tags[text] = tag;
			this._order.push(text);
			this._container.insertBefore(tag.getElement(), this._input.parentElement);
		},
		isEmpty: function () {
			return this.getText().length === 0;
		},
		isExist: function (tag) {
			return !!this._tags[tag];
		},
		getLastTag: function () {
			var last = this._order.length;
			if (last === 0) {
				return null;
			}
			var text = this._order[last - 1];
			return this._tags[text];
			
		},
		removeTag: function (tag) {
			var text = tag.getText();
			if (this.isExist(tag)) {
				throw new Error("Нельзя удалить метку которой нет :C");
			}
			
			delete this._tags[text];
			this._order.splice(this._order.indexOf(text), 1);
			this._container.removeChild(tag.getElement());
		},
		inputHandler: function (e) {
			if (!base.in(this.DELIMITERS, String.fromCharCode(e.charCode))) {
				return;
			}
			
			try {
				var tag = new Tag(this.getText(), this);
				this.addTag(tag);
				this.clear();
			} catch(e) {
				alert(e.message);
			}
			e.preventDefault(); //not print delimiter
		},
		backspaceHandler: function (e) {
			if (e.keyCode !== 8 || !this.isEmpty() || !this.getLastTag()) {
				return;
			}
			//backspace action
			this.removeTag(this.getLastTag());
		}
	});
})(window);