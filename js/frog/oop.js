/* Frog framework - oop package */

(function (exports) {
	var base = exports.frog.base.resolve("frog.base", exports);
	var oop = base.resolve("frog.oop", exports);

	oop.Class = function (data) {
		var TempClass = data.constructor || function () {};

		var ParentClass = data.extends;
		if (ParentClass) {
			if (!base.isFunction(ParentClass)) {
				throw new Error("Illegal extends attr value");
			}
			oop.extends(TempClass, ParentClass);
			//next line for parent() support
			TempClass.$$parent$$ = ParentClass;
		}


		base.forEach(data, function (key, val) {
			TempClass.prototype[key] = val;
			//next block for parent() support
			if (ParentClass) {
				TempClass.prototype[key].$$parent$$ = ParentClass.prototype[key]; 
			}
		}, function (key, val) {
			return /*base.isFunction(val) &&*/ !base.in(["constructor", "extends"], val);
		});

		TempClass.prototype.parent = function () {
			var parent = arguments.callee.caller.$$parent$$;
			if (!parent) {
				throw new Error("Parent method not found");
			}
			return parent.apply(this, arguments);
		}

		return TempClass;
	};

	oop.extends = function (childClass, parentClass) {
		childClass.prototype = Object.create(parentClass.prototype);
		childClass.prototype.constructor = childClass;
	};
})(window);