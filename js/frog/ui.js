/* Frog framework - ui package */

(function (exports) {
	var base = exports.frog.base.resolve("frog.base", exports);
	var oop = base.resolve("frog.oop", exports);
	var ui = base.resolve("frog.ui", exports);

	ui.render = function (template, data) {
		return template.replace(/\{\{(.*?)\}\}/g, function (full, value) {
			return data[value];
		});
	};
	
	ui.selector = function (selector) {
		return {
			_elements: document.querySelectorAll(selector),
			attach: function (Component) {
				var components = [];
				for (var i = 0; i < this._elements.length; i++) {
					var componentInstance = new Component();
					componentInstance.attach(this._elements[i])
					components.push(componentInstance);
				}
				return components;
			}
		}
	};
})(window);