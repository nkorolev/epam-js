var Purchase = frog.oop.Class({
	constructor: function (name, price, count) {
		this._name = name;
		this._price = price;
		this._count = count;
	},
	getName: function () {
		return this._name;
	},
	getPrice: function () {
		return this._price;
	},
	getCount: function () {
		return this._count;
	}, 
	getTotalCost: function () {
		return this._count * this._price;
	}
});

var FixedDiscountPurchase = frog.oop.Class({
	extends: Purchase,
	constructor: function (name, price, count, discount) {
		this.parent(name, price, count);
		this._discount = discount;
	}, 
	getDiscount: function () {
		return this._discount;
	},
	getTotalCost: function () {
		return this.parent() - this._discount;
	}
});

var p = new Purchase("Milk", 7850, 5);
var dp = new FixedDiscountPurchase("Milk", 7850, 5, 567);
console.log(dp.getTotalCost() + dp.getDiscount() === p.getTotalCost());